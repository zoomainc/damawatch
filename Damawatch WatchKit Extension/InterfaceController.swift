//
//  InterfaceController.swift
//  Damawatch WatchKit Extension
//
//  Created by JinhoYoo on 2015. 6. 27..
//  Copyright © 2015년 Zoomma. All rights reserved.
//

import WatchKit
import Foundation

class InterfaceController: WKInterfaceController
{
    var mbReverse = false
    var mbInit = true
    var mAnimTimer = 0.0
    var mMissionBarPerc = 0.0
    
    @IBOutlet var mAnim: WKInterfaceImage!
    @IBOutlet var mMissionBarFill: WKInterfaceGroup!
    
    override func awakeWithContext(context: AnyObject?)
    {
        super.awakeWithContext(context)
        
        NSTimer.scheduledTimerWithTimeInterval(
            1.0/30.0,
            target: self,
            selector: Selector("update"),
            userInfo: nil,
            repeats: true)

        mAnim.setImageNamed("Monster_")
        mAnim.startAnimating()
        
        mMissionBarFill.setWidth(CGFloat(70.0 * mMissionBarPerc))
    }

    override func willActivate()
    {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
    }

    override func didDeactivate()
    {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
    }
    
    func update()
    {
        if mbInit
        {
            if !mbReverse
            {
                mAnim.startAnimatingWithImagesInRange(NSMakeRange(0, 22), duration:  3.0, repeatCount: 1)
            }
            else
            {
                mAnim.startAnimatingWithImagesInRange(NSMakeRange(0, 22), duration: -3.0, repeatCount: 1)
            }
            mAnimTimer = 0.0
            mbInit = false
        }
        
        if mAnimTimer >= 3.0
        {
            mbInit = true
            mbReverse = !mbReverse
        }
        mAnimTimer += 1.0 / 30.0
        
        mMissionBarPerc = mAnimTimer
        mMissionBarFill.setWidth(CGFloat(70.0 * mMissionBarPerc / 3.0))
    }
}
