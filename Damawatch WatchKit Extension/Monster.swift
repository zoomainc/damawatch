//
//  Monster.swift
//  Damawatch
//
//  Created by Jong Yoon on 7/14/15.
//  Copyright © 2015 Zoomma. All rights reserved.
//

import WatchKit
import Foundation

class Monster
{
    let kImgName = "Monster_"
    
    var mrAnim: WKInterfaceImage
    var mState = EntityState.Idle
    var mStateTimer = 0.0
    var mbInit = true
    
    enum EntityState
    {
        case Idle
        case Transition
    }
    
    init(inout rAnim: WKInterfaceImage)
    {
        mrAnim = rAnim
    }
    
    func setState(state: EntityState)
    {
        if state != mState
        {
            mState = state
            mbInit = true
        }
    }
    
    func initState()
    {
        mbInit = false
        mStateTimer = 0.0
        
        switch mState
        {
        case .Idle:
            break
            
        case .Transition:
            break
        }
    }
    
    func updateState()
    {
        switch mState
        {
        case .Idle:
            break
            
        case .Transition:
            break
        }
    }
    
    func update()
    {
        if mbInit
        {
            initState()
        }
        updateState()
        
        mStateTimer += 1.0 / 30.0
    }
}